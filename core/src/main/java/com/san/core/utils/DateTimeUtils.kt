package com.san.core.utils

import java.text.SimpleDateFormat
import java.util.Date
import java.util.Locale

fun Long?.millisToDate(format: String = "yyyy-MM-dd HH:mm:ss"): String {
    val date = Date(this ?: Date().time)
    val sdf = SimpleDateFormat(format, Locale.ENGLISH)
    return sdf.format(date)
}