package com.san.core.base

import android.util.Log
import androidx.activity.ComponentActivity
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleEventObserver
import androidx.lifecycle.LifecycleOwner
import timber.log.Timber
import kotlin.reflect.KProperty

interface AppLogger {
    fun registerLifeCycleOwner(owner: LifecycleOwner)
}

class AppLoggerImpl : AppLogger, LifecycleEventObserver {
    override fun registerLifeCycleOwner(owner: LifecycleOwner) {
        owner.lifecycle.addObserver(this)
    }

    override fun onStateChanged(source: LifecycleOwner, event: Lifecycle.Event) {
        Timber.i("App State - ${event.name}")
    }

}