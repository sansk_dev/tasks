package com.san.pluto

import android.app.Application
import com.pluto.Pluto
import com.pluto.plugins.exceptions.PlutoExceptionsPlugin
import com.pluto.plugins.logger.PlutoLoggerPlugin
import com.pluto.plugins.network.PlutoNetworkPlugin
import com.pluto.plugins.preferences.PlutoSharePreferencesPlugin
import com.pluto.plugins.rooms.db.PlutoRoomsDatabasePlugin

open class PlutoApp : Application() {

    override fun onCreate() {
        super.onCreate()
        initPluto()

    }

    private fun initPluto() {
        Pluto.Installer(this)
            .addPlugin(PlutoNetworkPlugin())
            .addPlugin(PlutoLoggerPlugin())
            .addPlugin(PlutoExceptionsPlugin())
            .addPlugin(PlutoSharePreferencesPlugin())
            .addPlugin(PlutoRoomsDatabasePlugin())
            .install()
    }
}