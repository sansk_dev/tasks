package com.san.task.data


enum class Features(name: String) {
    NEWS("News"),
    CANVAS("Canvas"),
    BARD("Bard"),
    ACCOUNTS("Accounts")
}
