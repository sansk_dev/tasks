package com.san.news.data.repo

import android.content.Context
import com.san.core.utils.Resource
import com.san.core.utils.networkOnlyResource
import com.san.news.data.entity.NewsItem
import com.san.news.di.NewsApi
import com.san.news.di.NewsApiKtor
import com.san.news.domain.repo.NewsRepository
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class NewsRepoImplementation @Inject constructor(
    private val context: Context,
    private val api: NewsApi,
    private val newsApiKtor: NewsApiKtor,
) : NewsRepository {

    override suspend fun fetchNews(page: Int): Flow<Resource<List<NewsItem>>> {
        return networkOnlyResource(context) {
            //api.getNews(page = page)
            newsApiKtor.getNews()
        }
    }

}