package com.san.news.presentation.component

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.Card
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import coil.compose.AsyncImage
import com.san.news.data.entity.NewsItem

@Composable
fun NewsItem(news: NewsItem) {

    Card(
        Modifier
            .fillMaxWidth()
            .padding(horizontal = 12.dp, vertical = 6.dp),
        shape = MaterialTheme.shapes.medium,
    ) {
        Column(
            modifier = Modifier
                .fillMaxSize()
                .background(MaterialTheme.colorScheme.background.copy(alpha = .5f)),
            verticalArrangement = Arrangement.Top
        ) {
            AsyncImage(
                modifier = Modifier
                    .fillMaxWidth()
                    .height(150.dp),
                contentScale = ContentScale.FillBounds,
                model = news.image,
                contentDescription = news.image
            )

            Text(
                modifier = Modifier.padding(top = 8.dp, start = 4.dp, end = 4.dp, bottom = 2.dp),
                text = news.title ?: "Not found",
                color = MaterialTheme.colorScheme.secondary,
                maxLines = 1,
                style = MaterialTheme.typography.titleMedium
            )

            if (news.hasBody == true) {
                Text(
                    modifier = Modifier.padding(
                        top = 2.dp, start = 4.dp, end = 4.dp, bottom = 8.dp
                    ),
                    text = news.body ?: "Not found",
                    style = MaterialTheme.typography.bodyMedium,
                    color = MaterialTheme.colorScheme.secondary,
                    softWrap = true,
                    maxLines = 2,
                    overflow = TextOverflow.Ellipsis
                )
            }

        }

    }

}

@Preview
@Composable
fun NewsItemPreview() {
    NewsItem(
        news = NewsItem(
            id = "176a649c0a57287ae22a8ef7679170ff6f6c54b92fdee64a7fa900e354715066",
            body = "Storm surges are expected to impact much of the Southeast coast through tonight as Idalia continues its path sustaining tropical storm-force winds of up to 60 mph, according to an 11 p.m. ET update from the National Weather Service (NWS).\n\nCoastal flooding is also expected in Storm Surge Watch areas in North Carolina on Thursday, NWS noted its latest update.\n\nIdalia is currently 15 miles northwest of Charleston, South Carolina. Here's where you can track the storm's path.",
            hasBody = true,
            image = "https://lh5.googleusercontent.com/proxy/QBFiOUMBCInxqqCybpNy7likEJ5lYaQzn2JSv-uDvUBLRZSmD1FbWlxNgQFboFCi8NtyUlB1X-anNpgdwKTf3WOXz8R3AeXBWt1edeZcuf9Tvt7EmK7bdKzC2V5yNUAxAbFqNRAdW4dtgD_Wwo24x5DczXvqQS5uHx9H4Q=s1200",
            link = "https://www.cnn.com/us/live-news/hurricane-idalia-path-florida-08-30-23/index.html",
            publishedAt = "2023-08-31T04:40:52Z",
            publishedTimestamp = 1693456852,
            shortLink = "cnn.com",
            sourceName = "CNN",
            title = "Aug.30 Hurricane Idalia news "
        )
    )
}

