package com.san.news.di

import com.san.news.data.entity.NewsItem
import io.ktor.client.HttpClient
import io.ktor.client.call.body
import io.ktor.client.request.get
import io.ktor.client.request.parameter
import io.ktor.client.request.url
import retrofit2.http.GET
import retrofit2.http.Query
import javax.inject.Inject

interface NewsApi {

    // https://newsi-app.com/api/local?category=world&language=en&country=us&sort=top&page=1

    /*@GET("newslist/{page}.json")
    suspend fun getNews(@Path("page") page: Int = 0): NewsBaseResponseOld*/

    @GET("local")
    suspend fun getNews(
        @Query("category") category: String = "world",
        @Query("language") language: String = "en",
        @Query("country") country: String = "us",
        @Query("sort") sort: String = "top",
        @Query("page") page: Int = 1,
    ): List<NewsItem>
}

class NewsApiKtor @Inject constructor(private val client: HttpClient) : NewsApi {
    override suspend fun getNews(
        category: String,
        language: String,
        country: String,
        sort: String,
        page: Int
    ): List<NewsItem> {
        return client.get {
            url("https://newsi-app.com/api/local")
            parameter("category", "world")
            parameter("language", "en")
            parameter("country", "us")
            parameter("sort", "top")
            parameter("page", 1)
        }.body()
    }

}