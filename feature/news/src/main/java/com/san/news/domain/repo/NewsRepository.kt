package com.san.news.domain.repo

import com.san.core.utils.Resource
import com.san.news.data.entity.NewsItem
import kotlinx.coroutines.flow.Flow

interface NewsRepository {

    suspend fun fetchNews(page: Int = 0): Flow<Resource<List<NewsItem>>>
}