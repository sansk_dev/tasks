package com.san.news.data.entity


import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class NewsItem(
    @SerialName("body")
    val body: String? = null,
    @SerialName("hasBody")
    val hasBody: Boolean? = null,
    @SerialName("_id")
    val id: String? = null,
    @SerialName("image")
    val image: String? = null,
    @SerialName("link")
    val link: String? = null,
    @SerialName("publishedAt")
    val publishedAt: String? = null,
    @SerialName("publishedTimestamp")
    val publishedTimestamp: Int? = null,
    @SerialName("shortLink")
    val shortLink: String? = null,
    @SerialName("sourceName")
    val sourceName: String? = null,
    @SerialName("title")
    val title: String? = null
)