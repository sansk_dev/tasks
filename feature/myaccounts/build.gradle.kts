@Suppress("DSL_SCOPE_VIOLATION") // TODO: Remove once KTIJ-19369 is fixed
plugins {
    id("com.android.library")
    id("org.jetbrains.kotlin.android")
    id("io.realm.kotlin")
    id("kotlinx-serialization")
}

android {
    namespace = "com.san.myaccounts"
    compileSdk = 34

    defaultConfig {
        //applicationId = "com.san.myaccounts"
        minSdk = 23
        targetSdk = 33
        //versionCode = 1
        //versionName = "1.0"

        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
        vectorDrawables {
            useSupportLibrary = true
        }
    }

    buildTypes {
        release {
            isMinifyEnabled = false
            proguardFiles(
                getDefaultProguardFile("proguard-android-optimize.txt"),
                "proguard-rules.pro"
            )
        }
    }
    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_17
        targetCompatibility = JavaVersion.VERSION_17
    }
    kotlinOptions {
        jvmTarget = "17"
    }
    buildFeatures {
        compose = true
    }
    composeOptions {
        kotlinCompilerExtensionVersion = "1.5.1"
    }
    packaging {
        resources {
            excludes += "/META-INF/{AL2.0,LGPL2.1}"
        }
    }

    kotlin {
        jvmToolchain(17)
    }
    tasks.withType<org.jetbrains.kotlin.gradle.tasks.KotlinCompile>().configureEach {
        kotlinOptions {
            jvmTarget = "17"
        }
    }
}

dependencies {

    api(project(mapOf("path" to ":core")))
    implementation(libs.androidx.ktx)
    implementation(libs.androidx.lifecycle.runtime.ktx)
    implementation(libs.activity.compose)
    implementation(platform(libs.compose.bom))
    implementation(libs.ui)
    implementation(libs.ui.graphics)
    implementation(libs.ui.tooling.preview)
    implementation(libs.material3)
    testImplementation(libs.junit)
    androidTestImplementation(libs.androidx.junit)
    androidTestImplementation(libs.androidx.espresso.core)
    androidTestImplementation(platform(libs.compose.bom))
    androidTestImplementation(libs.ui.test.junit4)
    debugImplementation(libs.ui.tooling)
    debugImplementation(libs.ui.test.manifest)
    implementation(libs.androidx.material.icons.extended.android)

    implementation("androidx.lifecycle:lifecycle-viewmodel-compose")
    // Optional - Integration with LiveData
    implementation("androidx.compose.runtime:runtime-livedata")
    //Optional - Runtime compose
    implementation("androidx.lifecycle:lifecycle-runtime-compose")

    implementation ("io.realm.kotlin:library-base:1.11.0")
    implementation ("io.realm.kotlin:library-sync:1.11.0")

    /* val koin_version = "3.4.3"
     val koin_compose = "1.0.4"
     implementation("io.insert-koin:koin-core:$koin_version")
     implementation ("io.insert-koin:koin-android:$koin_version")
     implementation ("io.insert-koin:koin-androidx-navigation:$koin_version")
     implementation ("io.insert-koin:koin-androidx-compose:$koin_compose")
     // Koin Test features
     testImplementation("io.insert-koin:koin-test:$koin_version")*/
}