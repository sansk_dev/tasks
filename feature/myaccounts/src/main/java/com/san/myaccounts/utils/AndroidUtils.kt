package com.san.myaccounts.utils

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavHostController
import timber.log.Timber
import java.util.regex.Matcher
import java.util.regex.Pattern

inline fun <VM : ViewModel> viewModelFactory(crossinline f: () -> VM) =
    object : ViewModelProvider.Factory {
        @Suppress("UNCHECKED_CAST")
        override fun <T : ViewModel> create(modelClass: Class<T>): T = f() as T
    }


fun NavHostController?.safeNavigate(path: String?) {
    try {
        if (path != null) {
            this?.navigate(path)
        } else {
            throw (Throwable("Path should not be null"))
        }
    } catch (e: Exception) {
        Timber.tag("Compose_Nav").e(e.localizedMessage)
    }
}

fun String.args(vararg params: String?): String {
    val regexPattern = "\\{([^}]+)\\}"
    val pattern = Pattern.compile(regexPattern)
    val matcher = pattern.matcher(this)
    val result = StringBuffer()
    var count = 0

    while (matcher.find()) {
        val replacementValue =
            Matcher.quoteReplacement(params[count].toString())
        matcher.appendReplacement(result, replacementValue)
        count++
    }
    matcher.appendTail(result)
    return result.toString()
}