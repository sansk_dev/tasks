package com.san.myaccounts.data.repo

import com.san.myaccounts.data.dao.TransactionDao
import com.san.myaccounts.data.local.realm.objects.TransactionData

class TransactionRepo(private val dao: TransactionDao) {


    suspend fun createTransaction(data: TransactionData) {
        dao.createTransaction(data)
    }

    suspend fun getTransaction(bookId: String, tid: String) = dao.getTransaction(bookId, tid)

    suspend fun removeTransaction(tid: String) = dao.removeTransaction(tid)

    suspend fun observeTransactions(bookId: String) = dao.observeTransaction(bookId)


}