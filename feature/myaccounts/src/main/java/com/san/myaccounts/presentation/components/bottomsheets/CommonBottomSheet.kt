@file:OptIn(ExperimentalMaterial3Api::class)

package com.san.myaccounts.presentation.components.bottomsheets

import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.ModalBottomSheet
import androidx.compose.material3.SheetState
import androidx.compose.material3.SheetValue
import androidx.compose.material3.rememberModalBottomSheetState
import androidx.compose.runtime.Composable
import androidx.compose.runtime.rememberCoroutineScope
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch

@Composable
fun <T> CommonBottomSheetLayout(
    coroutineScope: CoroutineScope = rememberCoroutineScope(),
    modalSheetState: SheetState = rememberModalBottomSheetState(),
    data: T,
    content: @Composable (T) -> Unit,
) {
    if (modalSheetState.currentValue != SheetValue.Hidden) {

        ModalBottomSheet(
            sheetState = modalSheetState,
            onDismissRequest = {
                coroutineScope.launch { modalSheetState.hide() }
            },
            shape = MaterialTheme.shapes.extraSmall
        ) {
            content(data)
        }
    }
}