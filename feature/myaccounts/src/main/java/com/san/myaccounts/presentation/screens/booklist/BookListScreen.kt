package com.san.myaccounts.presentation.screens.booklist

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.twotone.AddCard
import androidx.compose.material.icons.twotone.Sort
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import androidx.navigation.NavHostController
import com.san.myaccounts.data.local.realm.objects.AccountBook
import com.san.myaccounts.presentation.components.AccountCard
import com.san.myaccounts.presentation.components.CommonDialog
import com.san.myaccounts.presentation.navigation.AccountsScreen
import com.san.myaccounts.utils.args
import com.san.myaccounts.utils.safeNavigate

@Composable
fun AccountBookListScreen(
    modifier: Modifier = Modifier,
    navHostController: NavHostController,
    viewModel: BookListViewModel
) {
    val accounts = viewModel.accountState.collectAsState().value
    var showDialog by remember {
        mutableStateOf(false)
    }
    var book by remember { mutableStateOf<AccountBook?>(null) }

    if (accounts.isLoading) {
        Column {
            CircularProgressIndicator()
        }
    } else {
        Column(modifier.fillMaxSize()) {
            Row(
                Modifier
                    .fillMaxWidth()
                    .padding(horizontal = 2.dp, vertical = 2.dp),
                horizontalArrangement = Arrangement.SpaceBetween
            ) {
                IconButton(onClick = { showDialog = true }) {
                    Icon(imageVector = Icons.TwoTone.AddCard, contentDescription = "add")
                }

                IconButton(onClick = { /*TODO*/ }) {
                    Icon(imageVector = Icons.TwoTone.Sort, contentDescription = "sort")
                }
            }
            LazyColumn(
                modifier
                    .fillMaxSize()
                    .padding(10.dp)
            ) {
                items(accounts.data) {
                    AccountCard(
                        account = it,
                        onRemove = { viewModel.removeAccount(it.bookId) },
                        onIconClick = {
                            book = it
                            showDialog = true
                        }) {
                        navHostController.safeNavigate(
                            AccountsScreen.BookDetails.route.args(
                                it.bookId,
                                it.name
                            )
                        )
                    }

                }
            }
        }

    }

    if (showDialog) {
        CommonDialog(data = book, onDismiss = {
            showDialog = false
            book = null
        }) {
            CreateBookWidget(data = it) { book ->
                viewModel.createUserAccountBook(book)
                showDialog = false
            }
        }
    }

}