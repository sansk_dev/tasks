package com.san.myaccounts.data.local.realm.daoimpl

import com.san.myaccounts.data.dao.AccountDao
import com.san.myaccounts.data.local.realm.RealmUtils
import com.san.myaccounts.data.local.realm.objects.AccountBook
import com.san.myaccounts.data.local.realm.objects.TransactionData
import io.realm.kotlin.ext.query
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import timber.log.Timber

class AccountDaoImpl(utils: RealmUtils) : AccountDao {

    private val realm = RealmUtils.realm

    override suspend fun observeAccounts(): Flow<List<AccountBook>> {
        return realm.query<AccountBook>().asFlow().map { it.list }
    }

    override suspend fun getUserAccounts(): List<AccountBook> {
        return realm.query<AccountBook>().find()
    }

    override suspend fun getUserAccount(id: String): AccountBook {
        return realm.query<AccountBook>("name == $0", id).first().find()!!
    }

    override suspend fun removeAccount(id: String): Boolean {
        return try {
            realm.writeBlocking {
                val obj = this.query<AccountBook>("bookId == $0", id).find()
                delete(obj)
            }
            true
        } catch (e: Exception) {
            false
        }
    }

    override suspend fun createUser(book: AccountBook): String {
        val s = realm.writeBlocking {
            try {
                copyToRealm(book)
                true
            } catch (e: IllegalArgumentException) {
                this.cancelWrite()
                false
            }
        }
        return if (!s) updateUser(book) else book.bookId
    }

    override suspend fun updateUser(book: AccountBook): String {
        return try {
            realm.writeBlocking {
                this.query<AccountBook>(
                    "bookId == $0",
                    book.bookId
                ).first().find()?.let {
                    it.apply {
                        this.name = book.name
                        this.description = book.description
                        this.image = book.image
                        this.isActive = book.isActive
                    }
                }
            }
            book.bookId
        } catch (e: Exception) {
            Timber.e(e)
            ""
        }
    }


}