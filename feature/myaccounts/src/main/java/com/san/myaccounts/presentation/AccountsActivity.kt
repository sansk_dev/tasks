package com.san.myaccounts.presentation

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import androidx.navigation.compose.rememberNavController
import com.san.myaccounts.presentation.navigation.MyAccountsNavigation
import com.san.myaccounts.presentation.screens.MyAccountsScreen
import com.san.myaccounts.presentation.theme.AccountsTheme

class AccountsActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            AccountsTheme {
                val navController = rememberNavController()
                // A surface container using the 'background' color from the theme
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    MyAccountsNavigation(navController = navController)
                    listenMessage(intent)
                }
            }
        }
    }

    override fun onNewIntent(intent: Intent?) {
        super.onNewIntent(intent)
        listenMessage(intent)

    }

    private fun listenMessage(intent: Intent?) {
        val msg = intent?.extras?.getString("sender")
        msg?.let {
            Toast.makeText(this, msg, Toast.LENGTH_LONG).show()
        }
    }
}

@Preview(showBackground = true)
@Composable
fun GreetingPreview() {
    AccountsTheme {
        MyAccountsScreen()
    }
}