package com.san.myaccounts.presentation.screens.book

import com.san.myaccounts.data.local.realm.objects.TransactionData

data class BookDetailsScreenState(
    var isLoading:Boolean = false,
    var data:List<TransactionData> = emptyList(),
)