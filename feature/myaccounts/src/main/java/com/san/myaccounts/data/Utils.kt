package com.san.myaccounts.data

import com.san.myaccounts.data.local.realm.objects.TransactionData

fun TransactionData.copy() = TransactionData().apply {
    this.transactionId = this@copy.transactionId
    this.title = this@copy.title
    this.date = this@copy.date
    this.dueDate = this@copy.dueDate
    this.amount = this@copy.amount
    this.note = this@copy.note
    this.status = this@copy.status
    this.bookId = this@copy.bookId
}

enum class TransactionStatus(val status: Int) {
    NEW(0),
    DONE(1),
    PENDING(2),
    CANCELLED(3),

}

enum class TransType(val status: Int) {
    SENT(0),
    RECEIVED(1),
}