package com.san.myaccounts.data.local.realm.daoimpl

import com.san.myaccounts.data.dao.TransactionDao
import com.san.myaccounts.data.local.realm.RealmUtils
import com.san.myaccounts.data.local.realm.objects.TransactionData
import io.realm.kotlin.ext.query
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import timber.log.Timber

class TransactionDaoImpl : TransactionDao {
    private val realm = RealmUtils.realm

    override suspend fun createTransaction(data: TransactionData): Boolean {
        val s = realm.writeBlocking {
            return@writeBlocking try {
                this.copyToRealm(data)
                true
            } catch (e: Exception) {
                this.cancelWrite()
                Timber.e(e)
                false
            }
        }
        return if (!s) updateTransaction(data) else true
    }

    private suspend fun updateTransaction(data: TransactionData): Boolean {
        return try {
            realm.write {
                this.query<TransactionData>(
                    "transactionId == $0",
                    data.transactionId
                ).first().find()?.let {
                    it.apply {
                        this.date = data.date
                        this.amount = data.amount
                        this.dueDate = data.dueDate
                        this.note = data.note
                        this.status = data.status
                        this.title = data.title
                        this.type = data.type
                    }
                }
            }
            true
        } catch (e: Exception) {
            Timber.e(e)
            false
        }
    }

    override suspend fun getTransactions(bookId: String): List<TransactionData> {
        TODO("Not yet implemented")
    }

    override suspend fun getTransaction(bookId: String, transactionId: String): TransactionData? {
        return realm.query<TransactionData>(
            "bookId == $0 && transactionId == $1",
            bookId,
            transactionId
        ).first().find()
    }

    override suspend fun observeTransaction(bookId: String): Flow<List<TransactionData>> {
        return realm.query<TransactionData>("bookId == $0", bookId).sort("date").asFlow()
            .map { it.list }
    }

    override suspend fun removeTransaction(id: String): Boolean {
        TODO("Not yet implemented")
    }
}