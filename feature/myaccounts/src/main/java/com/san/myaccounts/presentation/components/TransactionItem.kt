package com.san.myaccounts.presentation.components

import android.content.res.Configuration
import androidx.compose.animation.AnimatedVisibility
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.wrapContentHeight
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.tooling.preview.PreviewParameter
import androidx.compose.ui.tooling.preview.datasource.LoremIpsum
import androidx.compose.ui.unit.dp
import com.san.myaccounts.data.TransactionStatus
import com.san.myaccounts.data.local.realm.objects.TransactionData
import com.san.myaccounts.utils.generateRandomColor

@Composable
fun TransactionItem(
    data: TransactionData,
    modifier: Modifier = Modifier,
    onClick: (data: TransactionData) -> Unit
) {
    var isVisible by remember { mutableStateOf(false) }
    val color = remember { mutableStateOf(getColor(data.status)) }

    Row(
        modifier = modifier
            .fillMaxWidth()
            .wrapContentHeight()
            .padding(12.dp)
            .clickable {
                isVisible = !isVisible
            },
    ) {
        Avatar(text = data.status.take(1), color = color.value) { onClick(data) }
        Column(
            modifier = Modifier
                .fillMaxWidth()
                .padding(horizontal = 14.dp, vertical = 13.dp),
            horizontalAlignment = Alignment.Start,
            verticalArrangement = Arrangement.Center
        ) {
            Row(
                modifier = Modifier.fillMaxWidth(),
                horizontalArrangement = Arrangement.SpaceBetween,
                verticalAlignment = Alignment.CenterVertically
            ) {
                Text(
                    text = data.title,
                    style = MaterialTheme.typography.titleMedium,
                    color = MaterialTheme.colorScheme.secondary,
                    maxLines = 1,
                    overflow = TextOverflow.Ellipsis
                )
                Text(
                    text = data.amount,
                    style = MaterialTheme.typography.titleSmall,
                    color = MaterialTheme.colorScheme.primary
                )
            }
            Text(
                text = data.note,
                style = MaterialTheme.typography.bodySmall,
                maxLines = 2,
                overflow = TextOverflow.Ellipsis
            )

            AnimatedVisibility(isVisible) {
                Column {
                    Spacer(modifier = Modifier.padding(4.dp))
                    Text(
                        text = "Date: ${data.date}",
                        style = MaterialTheme.typography.bodyMedium
                    )
                    Text(
                        text = "Due Date: ${data.dueDate}",
                        style = MaterialTheme.typography.bodyMedium
                    )
                }
            }
        }
    }
}


@Composable
private fun Avatar(text: String, color: Color, onClick: () -> Unit) {
    Box(
        modifier = Modifier
            .size(50.dp)
            .clip(CircleShape)
            .background(color = color)
            .clickable { onClick() },
        contentAlignment = Alignment.Center
    ) {
        Text(
            text = text,
            style = MaterialTheme.typography.labelLarge,
            maxLines = 1,
            textAlign = TextAlign.Center,
            color = Color.White
        )
    }
}


@Preview(
    backgroundColor = 0xFFFFFFFF, showBackground = true, showSystemUi = true,
    uiMode = Configuration.UI_MODE_NIGHT_NO or Configuration.UI_MODE_TYPE_NORMAL
)
@Composable
fun PreviewTransactionItem(@PreviewParameter(LoremIpsum::class) text: String) {
    Column {
        TransactionItem(
            TransactionData().apply {
                title = text.take(14)
                date = "02/05/2023"
                dueDate = "02/06/2023"
                amount = "1000.00"
                note = text
                status = TransactionStatus.DONE.name
            }
        ) {

        }

        TransactionItem(
            TransactionData().apply {
                title = text.take(14)
                date = "02/05/2023"
                dueDate = "02/06/2023"
                amount = "1000.00"
                note = text.take(16)
                status = TransactionStatus.DONE.name
            }
        ) {}
    }
}

fun getColor(status: String): Color {
    return when (TransactionStatus.valueOf(status)) {
        TransactionStatus.NEW -> Color(0XFF228be6)
        TransactionStatus.DONE -> Color(0XFF40C057)
        TransactionStatus.PENDING -> Color(0XFFfab005)
        TransactionStatus.CANCELLED -> Color(0XFFfa5252)
    }
}