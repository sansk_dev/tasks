package com.san.myaccounts.presentation.navigation

import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.NavGraphBuilder
import androidx.navigation.NavHostController
import androidx.navigation.NavType
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.navArgument
import com.san.myaccounts.data.repo.AccountsRepo
import com.san.myaccounts.presentation.screens.MyAccountsScreen
import com.san.myaccounts.presentation.screens.book.BookDetailsPage
import com.san.myaccounts.presentation.screens.home.HomeScreen
import com.san.myaccounts.presentation.screens.booklist.AccountBookListScreen
import com.san.myaccounts.presentation.screens.booklist.BookListViewModel
import com.san.myaccounts.data.local.realm.daoimpl.AccountDaoImpl
import com.san.myaccounts.data.local.realm.RealmUtils
import com.san.myaccounts.data.local.realm.daoimpl.TransactionDaoImpl
import com.san.myaccounts.data.repo.TransactionRepo
import com.san.myaccounts.presentation.screens.book.BookDetailsViewModel
import com.san.myaccounts.presentation.screens.transactiondetail.TransactionDetailsScreen
import com.san.myaccounts.presentation.screens.transactiondetail.TransactionDetailsViewModel
import com.san.myaccounts.utils.viewModelFactory
import java.util.regex.Matcher
import java.util.regex.Pattern

sealed class AccountsScreen(
    var title: String,
    var route: String
) {
    companion object {
        const val bookId = "book_id"
        const val transId = "transaction_id"
        const val name = "name"
    }

    data object Main : AccountsScreen("Main", "accounts_main")

    /**
     * @param bookid
     * @param name
     */
    data object BookDetails : AccountsScreen("Book Details", "account_details/{$bookId}/{$name}")

    /**
     * @param bookId
     * @param transId
     */
    data object TransDetails :
        AccountsScreen("Trans Details", "transaction_details/{$bookId}/{$transId}")

}



@Composable
fun MyAccountsNavigation(navController: NavHostController) {
    NavHost(
        navController = navController, startDestination = AccountsScreen.Main.route
    ) {
        composable(AccountsScreen.Main.route) {
            MyAccountsScreen()
        }
    }
}


@Composable
fun MyAccountsMainNavigation(navController: NavHostController, innerPadding: PaddingValues) {
    NavHost(
        navController, startDestination = BottomNavItem.Home.route, Modifier.padding(innerPadding)
    ) {
        composable(BottomNavItem.Home.route) {
            HomeScreen()
        }

        accountsBookListNavGraph(navController)

        composable(BottomNavItem.Analytics.route) {
            Text(text = "analytics")
        }

        composable(BottomNavItem.Settings.route) {
            Text(text = "profile")
        }


    }
}


fun NavGraphBuilder.accountsBookListNavGraph(navController: NavHostController) {

    //parent
    composable(BottomNavItem.Books.route) {
        val viewModel =
            viewModel<BookListViewModel>(factory = viewModelFactory {
                BookListViewModel(
                    AccountsRepo(AccountDaoImpl(RealmUtils))
                )
            })
        AccountBookListScreen(navHostController = navController, viewModel = viewModel)
    }

    //child
    composable(route = AccountsScreen.BookDetails.route,
        arguments = listOf(
            navArgument(AccountsScreen.bookId) { type = NavType.StringType },
            navArgument(AccountsScreen.name) { type = NavType.StringType }
        )

    ) {
        val bookId = it.arguments?.getString(AccountsScreen.bookId) ?: ""
        val name = it.arguments?.getString(AccountsScreen.name) ?: ""

        val viewModel = viewModel<BookDetailsViewModel>(factory = viewModelFactory {
            BookDetailsViewModel(
                bookId,
                TransactionRepo(TransactionDaoImpl())
            )
        })
        BookDetailsPage(
            navController = navController,
            bookId = bookId,
            name = name,
            viewModel = viewModel
        )
    }

    composable(route = AccountsScreen.TransDetails.route,
        arguments = listOf(
            navArgument(AccountsScreen.bookId) { type = NavType.StringType },
            navArgument(AccountsScreen.transId) { type = NavType.StringType }
        )

    ) { navBackStackEntry ->
        val bookId = navBackStackEntry.arguments?.getString(AccountsScreen.bookId) ?: ""
        val transactionId = navBackStackEntry.arguments?.getString(AccountsScreen.transId) ?: ""

        val viewModel = viewModel<TransactionDetailsViewModel>(factory = viewModelFactory {
            TransactionDetailsViewModel(
                bookId,
                transactionId,
                TransactionRepo(TransactionDaoImpl())
            )
        })
        TransactionDetailsScreen(viewModel, { navController.navigateUp() }) {
            viewModel.createNewTransaction(it)
            navController.navigateUp()
        }
    }
}

