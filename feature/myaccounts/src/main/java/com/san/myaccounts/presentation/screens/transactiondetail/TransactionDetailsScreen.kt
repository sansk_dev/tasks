package com.san.myaccounts.presentation.screens.transactiondetail

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.material3.Button
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.LinearProgressIndicator
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Switch
import androidx.compose.material3.Text
import androidx.compose.material3.TopAppBar
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.lifecycle.viewmodel.compose.viewModel
import com.san.myaccounts.data.TransactionStatus
import com.san.myaccounts.data.local.realm.objects.TransactionData
import com.san.myaccounts.presentation.components.common.Dropdown
import com.san.myaccounts.presentation.components.common.InputText
import com.san.myaccounts.presentation.components.common.MyDatePicker

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun TransactionDetailsScreen(
    viewModel: TransactionDetailsViewModel,
    onBackClick: () -> Unit,
    onSaveClick: (data: TransactionData) -> Unit,
) {
    val state = viewModel.transactionDetailPageState.collectAsState().value
    val td by remember { mutableStateOf(state.data) }
    var selected by remember { mutableStateOf(td?.type.contentEquals("Sent", true) ) }

    Column {
        TopAppBar(title = {
            Row(
                Modifier.fillMaxWidth(),
                horizontalArrangement = Arrangement.SpaceBetween,
                verticalAlignment = Alignment.CenterVertically
            ) {
                Text(text = "Trans Info")
                Switch(
                    checked = selected,
                    onCheckedChange = {
                        selected = it
                        td?.type = if (it) "Sent" else "Received"
                    },
                    thumbContent = {
                        Text(
                            text = if (selected) "S" else "R",
                            style = MaterialTheme.typography.titleSmall
                        )
                    })
            }

        }, navigationIcon = {
            IconButton(onClick = { onBackClick() }) {
                Icon(imageVector = Icons.Default.ArrowBack, contentDescription = "back")
            }
        })
        if (state.isLoading) LinearProgressIndicator(Modifier.fillMaxWidth())

        Column(
            Modifier
                .fillMaxSize()
                .padding(16.dp),
            horizontalAlignment = Alignment.CenterHorizontally,
            verticalArrangement = Arrangement.Top
        ) {
            Dropdown(
                modifier = Modifier.fillMaxWidth(),
                label = "Status",
                value = td?.status,
                items = TransactionStatus.values().map { it.name }
            ) {
                td?.status = it
            }
            InputText(modifier = Modifier.fillMaxWidth(), label = "Title", value = td?.title) {
                td?.title = it
            }
            Row(
                modifier = Modifier.fillMaxWidth(),
                horizontalArrangement = Arrangement.SpaceEvenly
            ) {
                MyDatePicker(Modifier.weight(.5f), label = "Date", value = td?.date) {
                    td?.date = it
                }
                MyDatePicker(Modifier.weight(.5f), label = "Due Date", value = td?.dueDate) {
                    td?.dueDate = it
                }
            }

            InputText(
                modifier = Modifier.fillMaxWidth(),
                label = "Amount",
                value = td?.amount,
                kbo = KeyboardOptions(keyboardType = KeyboardType.Number)
            ) {
                td?.amount = it
            }
            InputText(
                modifier = Modifier
                    .fillMaxWidth()
                    .height(150.dp),
                label = "Note",
                value = td?.note,
                maxLines = 5
            ) {
                td?.note = it
            }

            Button(onClick = {
                td?.let { onSaveClick(it) }
            }) {
                Text(text = "Save")
            }
        }
    }


}

@Preview
@Composable
fun PreviewTDS() {
    TransactionDetailsScreen(viewModel(), {}) {

    }
}