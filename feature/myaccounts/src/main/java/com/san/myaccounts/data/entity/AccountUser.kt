package com.san.myaccounts.data.entity

data class AccountUser(
    val id: Int,
    val name: String,
    val amount: Int,
    val image: String? = null
)
