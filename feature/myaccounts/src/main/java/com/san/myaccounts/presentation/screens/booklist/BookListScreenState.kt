package com.san.myaccounts.presentation.screens.booklist

import com.san.myaccounts.data.local.realm.objects.AccountBook

data class BookListScreenState(
    var isLoading:Boolean = false,
    var data:List<AccountBook> = emptyList(),
)