package com.san.myaccounts.data.local.realm.objects


import io.realm.kotlin.types.RealmObject
import io.realm.kotlin.types.annotations.PrimaryKey
import kotlinx.serialization.Serializable
import java.util.UUID

@Serializable
open class AccountBook() : RealmObject {
    @PrimaryKey
    var bookId: String = UUID.randomUUID().toString()
    var isActive: Boolean = true
    var description: String = ""
    var name: String = ""
    var image: String = ""

    constructor(name: String) : this() {
        this.name = name
    }
}

fun AccountBook.copy() =
    AccountBook().apply {
        this.bookId = this@copy.bookId
        this.isActive = this@copy.isActive
        this.description = this@copy.description
        this.name = this@copy.name
        this.image = this@copy.image
    }