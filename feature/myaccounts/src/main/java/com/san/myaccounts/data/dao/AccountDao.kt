package com.san.myaccounts.data.dao

import com.san.myaccounts.data.local.realm.objects.AccountBook
import kotlinx.coroutines.flow.Flow

interface AccountDao {


    suspend fun createUser(book: AccountBook): String
    suspend fun updateUser(book: AccountBook): String

    suspend fun getUserAccounts(): List<AccountBook>
    suspend fun getUserAccount(id: String): AccountBook
    suspend fun observeAccounts(): Flow<List<AccountBook>>

    suspend fun removeAccount(id: String): Boolean
}