package com.san.myaccounts.presentation.screens.transactiondetail

import com.san.myaccounts.data.local.realm.objects.TransactionData

data class TransactionDetailsScreenState(
    var isLoading: Boolean = false,
    var data: TransactionData? = null,
)