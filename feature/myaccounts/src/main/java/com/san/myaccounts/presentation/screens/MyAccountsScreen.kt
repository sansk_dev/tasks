package com.san.myaccounts.presentation.screens

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material3.Scaffold
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.navigation.compose.rememberNavController
import com.san.myaccounts.presentation.components.navigation.BottomNavigation
import com.san.myaccounts.presentation.navigation.BottomNavItem
import com.san.myaccounts.presentation.navigation.MyAccountsMainNavigation
import com.san.myaccounts.presentation.navigation.bottomNavItems

@Composable
fun MyAccountsScreen(modifier: Modifier = Modifier, bni: List<BottomNavItem> = bottomNavItems) {
    val navController = rememberNavController()
    Column(modifier.fillMaxSize()) {

        Scaffold(bottomBar = {
            BottomNavigation(navController, items = bni)
        }) {
            MyAccountsMainNavigation(navController, innerPadding = it)
        }

    }
}

