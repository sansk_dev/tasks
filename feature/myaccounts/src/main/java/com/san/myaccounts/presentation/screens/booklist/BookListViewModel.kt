package com.san.myaccounts.presentation.screens.booklist

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.san.myaccounts.data.repo.AccountsRepo
import com.san.myaccounts.data.local.realm.objects.AccountBook
import com.san.myaccounts.data.local.realm.objects.copy
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch

class BookListViewModel(private val repo: AccountsRepo) : ViewModel() {

    private val _accountState = MutableStateFlow(BookListScreenState())
    val accountState: StateFlow<BookListScreenState> = _accountState


    init {
        fetchAccountBooks()
    }


    fun createUserAccountBook(book: AccountBook) {
        viewModelScope.launch {
            repo.createNewUserAccount(book)
        }
    }

    fun removeAccount(id: String) {
        viewModelScope.launch {
            repo.removeAccount(id)
        }
    }

    private fun fetchAccountBooks() {
        viewModelScope.launch {
            _accountState.value = _accountState.value.copy(isLoading = true, data = emptyList())
            repo.observeAccountsList().collectLatest { list ->
                _accountState.value =
                    _accountState.value.copy(isLoading = false, data = list.map { it.copy() })
            }
        }
    }

}