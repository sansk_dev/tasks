package com.san.myaccounts.presentation.components.common

import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material3.Button
import androidx.compose.material3.DatePicker
import androidx.compose.material3.DatePickerDialog
import androidx.compose.material3.DropdownMenuItem
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.ExposedDropdownMenuBox
import androidx.compose.material3.ExposedDropdownMenuDefaults
import androidx.compose.material3.OutlinedTextField
import androidx.compose.material3.Text
import androidx.compose.material3.rememberDatePickerState
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.focus.FocusRequester
import androidx.compose.ui.focus.focusRequester
import androidx.compose.ui.focus.onFocusEvent
import androidx.compose.ui.unit.dp
import com.san.core.utils.millisToDate


@Composable
fun InputText(
    modifier: Modifier = Modifier,
    label: String,
    value: String? = "",
    readOnly: Boolean = false,
    maxLines: Int = 1,
    kbo: KeyboardOptions = KeyboardOptions.Default,
    cb: (out: String) -> Unit
) {
    var text by remember {
        mutableStateOf(value)
    }
    var isError by remember {
        mutableStateOf(false)
    }
    OutlinedTextField(
        modifier = modifier.padding(4.dp),
        value = text ?: "NA",
        onValueChange = {
            text = it
            cb(it)
            isError = it.isEmpty() || it.isBlank()
        },
        label = { Text(text = label) },
        isError = isError,
        readOnly = readOnly,
        maxLines = maxLines,
        keyboardOptions = kbo
    )
}


@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun Dropdown(
    modifier: Modifier = Modifier,
    label: String,
    value: String? = "",
    items: List<String> = emptyList(),
    kbo: KeyboardOptions = KeyboardOptions.Default,
    cb: (out: String) -> Unit
) {
    var text by remember {
        mutableStateOf(value)
    }
    var isExpanded by remember {
        mutableStateOf(false)
    }

    ExposedDropdownMenuBox(
        modifier = modifier.padding(4.dp),
        expanded = isExpanded,
        onExpandedChange = { isExpanded = it }) {

        OutlinedTextField(
            modifier = Modifier.fillMaxWidth().menuAnchor(),
            value = text ?: "NA",
            onValueChange = {},
            label = { Text(text = label) },
            readOnly = true,
            maxLines = 2,
            keyboardOptions = kbo,
            trailingIcon = { ExposedDropdownMenuDefaults.TrailingIcon(expanded = isExpanded) },
        )

        ExposedDropdownMenu(
            expanded = isExpanded,
            onDismissRequest = { isExpanded = false }
        ) {
            items.forEach { item ->
                DropdownMenuItem(
                    text = { Text(text = item) },
                    onClick = {
                        text = item
                        cb(item)
                        isExpanded = false
                    }
                )
            }
        }
    }
}

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun MyDatePicker(
    modifier: Modifier, label: String, value: String? = "", callBack: (dt: String) -> Unit
) {
    val dps = rememberDatePickerState()
    val focusRequester = remember { FocusRequester() }

    var text by remember {
        mutableStateOf(value ?: "")
    }
    var isError by remember {
        mutableStateOf(false)
    }

    var showDialog by remember {
        mutableStateOf(false)
    }

    OutlinedTextField(modifier = modifier
        .focusRequester(focusRequester)
        .onFocusEvent {
            if (it.isFocused) {
                showDialog = true
            }
        }
        .padding(4.dp), value = text, onValueChange = {
        text = it
        isError = it.isEmpty() || it.isBlank()
    }, label = { Text(text = label) }, isError = isError, readOnly = true
    )

    if (showDialog) {
        DatePickerDialog(onDismissRequest = { showDialog = false }, confirmButton = {
            Button(onClick = {
                text = dps.selectedDateMillis.millisToDate("yyyy-MM-dd")
                callBack(text)
                showDialog = false
                focusRequester.freeFocus()
            }) {
                Text(text = "Ok")
            }
        }) {
            DatePicker(state = dps)
        }
    }

}