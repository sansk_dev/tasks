package com.san.myaccounts.presentation.components

import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.wrapContentSize
import androidx.compose.material3.AlertDialog
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun <T> CommonDialog(
    modifier: Modifier = Modifier,
    data: T,
    onDismiss: () -> Unit,
    content: @Composable (T) -> Unit,
) {
    AlertDialog(modifier = modifier
        .wrapContentSize()
        .padding(4.dp), onDismissRequest = onDismiss) {
        content(data)
    }
}

