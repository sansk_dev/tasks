package com.san.myaccounts.presentation.navigation

import androidx.compose.foundation.layout.RowScope
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.AccountCircle
import androidx.compose.material.icons.filled.Analytics
import androidx.compose.material.icons.filled.Class
import androidx.compose.material.icons.filled.Home
import androidx.compose.material.icons.filled.LibraryBooks
import androidx.compose.material.icons.filled.ListAlt
import androidx.compose.material.icons.filled.Settings
import androidx.compose.material.icons.outlined.AccountCircle
import androidx.compose.material.icons.outlined.Analytics
import androidx.compose.material.icons.outlined.Class
import androidx.compose.material.icons.outlined.Home
import androidx.compose.material.icons.outlined.LibraryBooks
import androidx.compose.material.icons.outlined.ListAlt
import androidx.compose.material.icons.outlined.Settings
import androidx.compose.material3.Icon
import androidx.compose.material3.NavigationBarItem
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.graphics.vector.ImageVector

val bottomNavItems = listOf(
    BottomNavItem.Home,
    BottomNavItem.Books,
    BottomNavItem.Analytics,
    BottomNavItem.Settings
)

sealed class BottomNavItem(
    var title: String,
    var icon: List<ImageVector>,
    var route: String
) {
    data object Home :
        BottomNavItem(
            "Home",
            listOf(Icons.Filled.Home, Icons.Outlined.Home),
            "accounts_home"
        )

    data object Books :
        BottomNavItem(
            "Books",
            listOf(Icons.Filled.LibraryBooks, Icons.Outlined.LibraryBooks),
            "accounts_book_list"
        )

    data object Analytics :
        BottomNavItem(
            "Analytics",
            listOf(Icons.Filled.Analytics, Icons.Outlined.Analytics),
            "accounts_analytics"
        )

    data object Settings :
        BottomNavItem(
            "Settings",
            listOf(Icons.Filled.Settings, Icons.Outlined.Settings),
            "accounts_settings"
        )
}

@Composable
fun RowScope.AddItem(
    screen: BottomNavItem,
    isSelected: Boolean = false,
    onClick: () -> Unit
) {
    NavigationBarItem(
        selected = isSelected,
        icon = {
            Icon(
                imageVector = if (isSelected) screen.icon[0] else screen.icon[1],
                contentDescription = screen.title
            )
        },
        label = {
            Text(text = screen.title)
        },
        onClick = {
            onClick()
        }
    )
}