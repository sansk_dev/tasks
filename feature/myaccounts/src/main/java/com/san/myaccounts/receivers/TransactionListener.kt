package com.san.myaccounts.receivers

import android.annotation.SuppressLint
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.provider.Telephony
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import androidx.core.app.TaskStackBuilder
import androidx.core.os.bundleOf
import com.san.myaccounts.R
import com.san.myaccounts.presentation.AccountsActivity


class TransactionListener : BroadcastReceiver() {


    override fun onReceive(context: Context?, intent: Intent?) {
        if (intent?.action?.equals(Telephony.Sms.Intents.SMS_RECEIVED_ACTION) == true) {


            Telephony.Sms.Intents.getMessagesFromIntent(intent)?.forEach { msg ->

                val sender = msg.displayOriginatingAddress
                val messageBody = msg.messageBody

                // Handle the incoming message
                // You can display it, log it, or perform any other actions here

                validateMessage(messageBody) {
                    if (it) triggerNotification(context, bundleOf("sender" to sender))
                }


            }
        }
    }


    private fun validateMessage(msg: String, callBack: (isValid: Boolean) -> Unit) {
        callBack(msg.contains("debit", true) || msg.contains("credit", true))
    }


    @SuppressLint("MissingPermission")
    private fun triggerNotification(context: Context?, bundle: Bundle) {

        context?.createNotificationChannel()

        val builder = NotificationCompat.Builder(context!!, "TE")
            .setSmallIcon(R.drawable.ic_launcher_foreground)
            .setContentTitle("My Accounts")
            .setContentText("New Transaction Detected - ${bundle.getString("sender")}")
            .setPriority(NotificationCompat.PRIORITY_HIGH)
            .setAutoCancel(true)


        // Create an intent to launch your activity when the notification is tapped
        val resultIntent = Intent(context, AccountsActivity::class.java)
        resultIntent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK
        resultIntent.putExtras(bundle)
        val stackBuilder = TaskStackBuilder.create(context)
        stackBuilder.addNextIntentWithParentStack(resultIntent)
        val resultPendingIntent =
            stackBuilder.getPendingIntent(0, PendingIntent.FLAG_IMMUTABLE)
        builder.setContentIntent(resultPendingIntent)

        with(NotificationManagerCompat.from(context)) {
            notify(1, builder.build())
        }

    }

    private fun Context.createNotificationChannel() {
        // Check if the notification channel already exists (required for Android 8.0 and higher)
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            val name = "My Accounts"
            val description = "Tracking Transactions"
            val importance = NotificationManager.IMPORTANCE_DEFAULT
            val channel = NotificationChannel("TE", name, importance).apply {
                this.description = description
                enableLights(true)
                lightColor = Color.RED
            }

            // Register the notification channel with the system
            val notificationManager =
                getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            notificationManager.createNotificationChannel(channel)
        }
    }
}