package com.san.myaccounts.data.dao

import com.san.myaccounts.data.local.realm.objects.TransactionData
import kotlinx.coroutines.flow.Flow

interface TransactionDao {


    suspend fun createTransaction(data: TransactionData): Boolean

    suspend fun getTransactions(bookId: String): List<TransactionData>
    suspend fun getTransaction(bookId: String, transactionId: String): TransactionData?
    suspend fun observeTransaction(bookId: String):  Flow<List<TransactionData>?>?

    suspend fun removeTransaction(tid: String): Boolean


}