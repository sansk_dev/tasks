package com.san.myaccounts.presentation.screens.book

import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.wrapContentHeight
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.pager.HorizontalPager
import androidx.compose.foundation.pager.rememberPagerState
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Add
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.FloatingActionButton
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Tab
import androidx.compose.material3.TabRow
import androidx.compose.material3.TabRowDefaults
import androidx.compose.material3.TabRowDefaults.tabIndicatorOffset
import androidx.compose.material3.Text
import androidx.compose.material3.TopAppBar
import androidx.compose.material3.TopAppBarDefaults
import androidx.compose.material3.rememberTopAppBarState
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.input.nestedscroll.nestedScroll
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.navigation.NavHostController
import androidx.navigation.compose.rememberNavController
import com.san.myaccounts.data.local.realm.objects.TransactionData
import com.san.myaccounts.presentation.components.TransactionItem
import com.san.myaccounts.presentation.navigation.AccountsScreen
import com.san.myaccounts.utils.args
import com.san.myaccounts.utils.safeNavigate
import kotlinx.coroutines.launch

@OptIn(ExperimentalFoundationApi::class, ExperimentalMaterial3Api::class)
@Composable
fun BookDetailsPage(
    modifier: Modifier = Modifier,
    navController: NavHostController,
    bookId: String?,
    name: String,
    viewModel: BookDetailsViewModel? = null
) {
    val tabTitles = listOf("All", "Sent", "Received")

    val pagerState = rememberPagerState(
        pageCount = { tabTitles.size }, initialPage = 0
    )
    val selectedTabIndex = pagerState.currentPage
    val coroutineScope = rememberCoroutineScope()
    val scrollBehavior = TopAppBarDefaults.enterAlwaysScrollBehavior(rememberTopAppBarState())

    val data = viewModel?.accountState?.collectAsState()?.value?.data ?: emptyList()

    Scaffold(
        modifier = modifier.nestedScroll(scrollBehavior.nestedScrollConnection),
        topBar = {
            TopAppBar(
                title = {
                    Text(
                        text = "$name Account",
                        style = MaterialTheme.typography.headlineSmall
                    )
                },
                navigationIcon = {
                    IconButton(
                        onClick = {
                            navController.navigateUp()
                        }
                    ) {
                        Icon(Icons.Default.ArrowBack, contentDescription = "back")
                    }
                },
                scrollBehavior = scrollBehavior
            )

        },
        floatingActionButton = {
            FloatingActionButton(onClick = {
                coroutineScope.launch {
                    //navController.navigate("transaction_details/${bookId}/-1")
                    navController.safeNavigate(AccountsScreen.TransDetails.route.args(bookId, "-1"))
                }
            }) {
                Icon(imageVector = Icons.Default.Add, contentDescription = "Add")
            }
        }) { pd ->
        Column(modifier = Modifier.padding(pd)) {

            TabRow(modifier = Modifier.wrapContentHeight(),
                selectedTabIndex = selectedTabIndex,
                contentColor = Color.White,
                containerColor = MaterialTheme.colorScheme.background,
                indicator = { tabs ->
                    TabRowDefaults.Indicator(
                        modifier = Modifier.tabIndicatorOffset(tabs[selectedTabIndex]),
                    )
                }) {
                tabTitles.forEachIndexed { index, title ->
                    Tab(selected = selectedTabIndex == index,
                        selectedContentColor = MaterialTheme.colorScheme.onBackground,
                        onClick = {
                            coroutineScope.launch {
                                pagerState.animateScrollToPage(index)
                            }
                        }) {
                        Text(
                            modifier = Modifier.padding(15.dp),
                            text = title,
                            maxLines = 1,
                            style = MaterialTheme.typography.titleSmall
                        )
                    }
                }
            }

            HorizontalPager(
                state = pagerState
            ) {
                Column(
                    modifier = Modifier.fillMaxSize(),
                    verticalArrangement = Arrangement.Top,
                    horizontalAlignment = Alignment.CenterHorizontally
                ) {
                    LazyColumn {
                        items(items = data.filter(selectedTabIndex), key = { it.transactionId }) {
                            TransactionItem(it) {
                                coroutineScope.launch {
                                    //navController.navigate("transaction_details/${it.bookId}/${it.transactionId}")
                                    navController.safeNavigate(
                                        AccountsScreen.TransDetails.route.args(
                                            it.bookId,
                                            it.transactionId
                                        )
                                    )
                                }
                            }

                        }
                    }
                }
            }
        }
    }
}

@Preview
@Composable
fun AccountsDetailsPagePreview() {
    BookDetailsPage(navController = rememberNavController(), bookId = "0", name = "San")
}

fun List<TransactionData>.filter(type: Int = 0) = this.filter {
    when (type) {
        0 -> true
        1 -> it.type == "Sent"
        2 -> it.type == "Received"
        else -> false
    }
}
