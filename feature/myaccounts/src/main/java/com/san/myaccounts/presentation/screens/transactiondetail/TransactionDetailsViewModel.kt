package com.san.myaccounts.presentation.screens.transactiondetail

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.san.myaccounts.data.copy
import com.san.myaccounts.data.local.realm.objects.TransactionData
import com.san.myaccounts.data.repo.TransactionRepo
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch

class TransactionDetailsViewModel(
    private val bookId: String,
    private val transactionId: String? = null,
    private val repo: TransactionRepo
) : ViewModel() {

    private val _state = MutableStateFlow(TransactionDetailsScreenState())
    val transactionDetailPageState: StateFlow<TransactionDetailsScreenState> = _state

    init {
        loadTransaction()
    }

    private fun loadTransaction() {
        _state.value = _state.value.copy(isLoading = true)
        viewModelScope.launch {
            val t = getTransaction(bookId, transactionId ?: "") ?: TransactionData(bookId, null)
            _state.value = _state.value.copy(isLoading = false, data = t.copy())
        }
    }

    fun createNewTransaction(data: TransactionData) {
        viewModelScope.launch {
            repo.createTransaction(data)
        }
    }

    private suspend fun getTransaction(bookId: String, transactionId: String): TransactionData? {
        return repo.getTransaction(bookId, transactionId)
    }

    fun removeTransaction(id: String) {
        viewModelScope.launch {
            repo.removeTransaction(id)
        }
    }

}