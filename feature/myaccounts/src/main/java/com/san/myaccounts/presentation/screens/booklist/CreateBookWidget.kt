package com.san.myaccounts.presentation.screens.booklist

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.wrapContentHeight
import androidx.compose.material3.Button
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.shadow
import androidx.compose.ui.unit.dp
import com.san.myaccounts.data.local.realm.objects.AccountBook
import com.san.myaccounts.presentation.components.common.InputText

@Composable
fun CreateBookWidget(
    modifier: Modifier = Modifier,
    data: AccountBook? = null,
    callBack: (book: AccountBook) -> Unit
) {
    val book by remember { mutableStateOf(data ?: AccountBook()) }

    Column(
        modifier = modifier
            .fillMaxWidth()
            .background(MaterialTheme.colorScheme.background, MaterialTheme.shapes.medium)
            .padding(4.dp),
        horizontalAlignment = Alignment.CenterHorizontally,
        verticalArrangement = Arrangement.Center
    ) {
        InputText(label = "Name", value = book.name) {
            book.name = it
        }
        InputText(label = "Description", value = book.description) {
            book.description = it
        }
        Button(onClick = { callBack(book) }) {
            Text(text = "Save")
        }
    }

}