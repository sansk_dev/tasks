package com.san.myaccounts.data.local.realm

import com.san.myaccounts.data.local.realm.objects.AccountBook
import com.san.myaccounts.data.local.realm.objects.TransactionData
import io.realm.kotlin.Realm
import io.realm.kotlin.RealmConfiguration
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import timber.log.Timber

object RealmUtils {
    val config =
        RealmConfiguration.Builder(schema = setOf(AccountBook::class, TransactionData::class))
            .schemaVersion(3)
            .build()

    val realm: Realm = Realm.open(config)

    init {
        Timber.v("Realm Path: ${config.path}")
    }


    fun close() = realm.close()


    val job = CoroutineScope(Dispatchers.IO).launch {

        /*val itemsFlow = items.asFlow()
        itemsFlow.collect { changes: ResultsChange<AccountBook> ->
            when (changes) {
                is UpdatedResults -> {
                    changes.insertions // indexes of inserted objects
                    changes.insertionRanges // ranges of inserted objects
                    changes.changes // indexes of modified objects
                    changes.changeRanges // ranges of modified objects
                    changes.deletions // indexes of deleted objects
                    changes.deletionRanges // ranges of deleted objects
                    changes.list // the full collection of objects
                }

                else -> {
                }
            }
        }*/
    }
}