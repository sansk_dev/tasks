package com.san.myaccounts.presentation.screens.book

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.san.myaccounts.data.repo.TransactionRepo
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch

class BookDetailsViewModel(bookId: String, private val repo: TransactionRepo) :
    ViewModel() {

    private val _accountState = MutableStateFlow(BookDetailsScreenState())
    val accountState: StateFlow<BookDetailsScreenState> = _accountState


    init {
        getBookDetails(bookId)
    }

    private fun getBookDetails(id: String) {
        viewModelScope.launch {
            _accountState.value = _accountState.value.copy(isLoading = true)
            repo.observeTransactions(id)?.collectLatest { list ->
                list?.let {
                    _accountState.value = _accountState.value.copy(isLoading = false, data = it)
                }
            }
        }
    }

}