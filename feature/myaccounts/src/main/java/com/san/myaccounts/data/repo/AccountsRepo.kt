package com.san.myaccounts.data.repo

import com.san.myaccounts.data.local.realm.objects.AccountBook
import com.san.myaccounts.data.dao.AccountDao
import io.realm.kotlin.ext.asFlow
import kotlinx.coroutines.flow.Flow

class AccountsRepo(private val accountDao: AccountDao) {
    suspend fun createNewUserAccount(book: AccountBook): String {
        return accountDao.createUser(book)
    }

    suspend fun observeAccountsList(): Flow<List<AccountBook>> = accountDao.observeAccounts()

    suspend fun removeAccount(id: String) = accountDao.removeAccount(id)

    suspend fun getBookDetails(id: String) = accountDao.getUserAccount(id).asFlow()
}