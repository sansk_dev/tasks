package com.san.myaccounts.data.local.realm.objects

import com.san.myaccounts.data.TransType
import com.san.myaccounts.data.TransactionStatus
import io.realm.kotlin.types.RealmObject
import io.realm.kotlin.types.annotations.PrimaryKey
import java.util.UUID

open class TransactionData() : RealmObject {
    @PrimaryKey
    var transactionId: String = ""
    var bookId: String = ""
    var title: String = ""
    var date: String = ""
    var dueDate: String = ""
    var amount: String = ""
    var note: String = ""
    var status: String = TransactionStatus.NEW.name
    var type: String = TransType.SENT.name

    constructor(bookId: String, transactionId: String? = null) : this() {
        this.bookId = bookId
        this.transactionId = transactionId ?: UUID.randomUUID().toString()
    }
}

