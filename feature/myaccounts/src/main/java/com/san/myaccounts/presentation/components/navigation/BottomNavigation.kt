package com.san.myaccounts.presentation.components.navigation

import androidx.compose.material3.BottomAppBar
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.ui.tooling.preview.Preview
import androidx.navigation.NavController
import androidx.navigation.NavDestination.Companion.hierarchy
import androidx.navigation.NavGraph.Companion.findStartDestination
import androidx.navigation.compose.currentBackStackEntryAsState
import androidx.navigation.compose.rememberNavController
import com.san.myaccounts.presentation.navigation.AddItem
import com.san.myaccounts.presentation.navigation.BottomNavItem
import com.san.myaccounts.presentation.navigation.bottomNavItems

@Composable
fun BottomNavigation(navController: NavController, items: List<BottomNavItem>) {
    val navBackStackEntry by navController.currentBackStackEntryAsState()
    val currentDestination = navBackStackEntry?.destination

    if (items.map { it.route }.contains(currentDestination?.route))
        BottomAppBar {
            items.forEach { screen ->
                AddItem(
                    screen = screen,
                    isSelected = currentDestination?.hierarchy?.any { it.route == screen.route } == true,
                ) {
                    kotlin.runCatching {
                        navController.navigate(screen.route) {
                            popUpTo(navController.graph.findStartDestination().id) {
                                saveState = true
                            }
                            launchSingleTop = true
                            restoreState = true
                        }
                    }
                }
            }
        }

}

@Preview
@Composable
fun PreviewBottomNav() {
    BottomNavigation(rememberNavController(), bottomNavItems)
}