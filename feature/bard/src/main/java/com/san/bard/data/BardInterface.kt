package com.san.bard.data

interface BardInterface {

    fun ask(input:String): String
}